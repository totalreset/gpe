#!/bin/env python3
# GPE - GPG Python Encrypter 

import re, os, subprocess
import PySimpleGUI as sg
import GPE

print("GPE started...")

## Loading available pub GPG keys from keyring
pub_keys = os.popen("gpg -k").readlines()
expired_keys = [ k.strip() for k in pub_keys if "expired" in k ]
valid_keys = [ k.strip() for k in pub_keys if "expired" not in k ]

## Clean strings
neat_keys = []
for k in valid_keys:
    k = re.sub(r'^.*? <', '', k)
    k = re.sub('>', '', k)
    neat_keys.append(k)
for k in expired_keys:
    k = re.sub(r'^.*? <', '', k)
    k = re.sub('>', '', k)
    neat_keys.append(k + " - Expired key")

## Creates list with emails and UIDs from available  GPG keys
UIDs = [ e for e in neat_keys if "@" not in e ]
emails = [ e for e in neat_keys if "@" in e ]
emails_uid = list(zip(emails, UIDs))


## Color theme
sg.theme('Material1')

## Gui layout -- Elements of the interface
layout = [
    [sg.T("File to encrypt:")],
    [sg.In(key='file')],
    [sg.FileBrowse(target='file', enable_events=True, key='file')],
    [sg.T("Recipients list:")],
    [sg.Combo(emails, enable_events=True, key='rec')],
    [sg.Button('Add key'), sg.Button('Remove key'), sg.Button('Check List')],
    [sg.Button('Encrypt')],
    [sg.T("Output: ")],
    [sg.Multiline(size=(50,8), key='multiline')],
    [sg.T("GPE - GPG Python Encrypter GUI. 2022.")],
]

## Events loop - Starting GUI
window = sg.Window('GPE', layout)
gpe = GPE.GPE(window)
while True:
    event, values = window.read()
    if values is not None and 'file' in values:
        file = values['file']
    else:
        file = ''
    if values is not None and 'rec' in values:
        recipients = values['rec']
    else:
        recipients = []

## Buttons functions
    if event == "file":
        print("File selected")
        window['multiline'].print("File selected")
    elif event == "Add key":
        gpe.add(recipients)
    elif event == "Remove key":
        gpe.remove(recipients)
    elif event == "Check List":
        gpe.checklist(recipients,file)
    elif event == 'Encrypt':
        gpe.encrypt(file)
    elif event == sg.WIN_CLOSED:
        print("Bye!")
        break
